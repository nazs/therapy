import Vue from 'vue';
import Vuex from 'vuex';
import mutations from './mutations';
import state from './state';
import getters from './getters';
import actions from './actions';
// import VueResource from 'vue-resource'
Vue.use(Vuex);
// Vue.use(VueResource);

export const store = new Vuex.Store({
  // strict: true,
  state,
  getters,
  mutations,
  actions
});
