import home from './components/home.vue';
import aboutus from './components/about.vue';
import therapies from './components/therapies.vue';
import contactus from './components/contact.vue';
import posts from './components/posts.vue';
import eachPost from './components/eachPost.vue';
import routes from './components/routes.vue';
import admin from './components/admin.vue';
// import understand from './components/understand.vue';
// import posts from './components/posts.vue';

export default[
  { path: '/', component: home},
  { path: '/aboutus', component: aboutus},
  { path: '/therapies', component: therapies},
  { path: '/contactus', component: contactus},
  { path: '/posts', component: posts},
  { path: '/eachPost/:title', component: eachPost },
  { path: '/routes/:title', component: routes },
  { path: '/admin', component: admin}
]


// { path: '/selfmast', component: selfmast },
// { path: '/', component: },
// { path: '/', component: },
// { path: '/', component: }
