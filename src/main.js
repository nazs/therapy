import Vue from 'vue'
import Vuex from 'vuex'
import Vuetify from 'vuetify'
import App from './App.vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import Routes from './routes'
import { store } from './store/store'
import { VueEditor } from 'vue2-editor'
// import Vueditor, {createEditor} from '../node_modules/vueditor/dist/script/vueditor.min.js'
// import 'vueditor/dist/style/vueditor.min.css'
let config = {

  // buttons on the toolbar, you can use '|' or 'divider' as the separator
  toolbar: [
    'removeFormat', 'undo', '|', 'elements', 'fontName', 'fontSize', 'foreColor', 'backColor', 'divider',
    'bold', 'italic', 'underline', 'strikeThrough', 'links', 'divider', 'subscript', 'superscript',
    'divider', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', '|', 'indent', 'outdent',
    'insertOrderedList', 'insertUnorderedList'
    // , '|', 'emoji', 'picture', 'tables', '|', 'switchView'
  ],

  // the font-family select's options, "val" refer to the actual css value, "abbr" refer to the option's text
  // "abbr" is optional when equals to "val";
  fontName: [
    {val: "", abbr: ""},
    {val: "arial black"}, {val: "times new roman"}, {val: "Courier New"}
  ],

  // the font-size select's options
  fontSize: ['12px', '14px', '16px', '18px', '0.8rem', '1.0rem', '1.2rem', '1.5rem', '2.0rem'],

  // the emoji list, you can get full list here: http://unicode.org/emoji/charts/full-emoji-list.html
  emoji: ["1f600", "1f601", "1f602", "1f923", "1f603"],

  // default is Chinese, to set to English use lang: 'en'
  // lang: 'en',

  // mode options: default | iframe
  mode: 'default',

  // if mode is set to 'iframe', specify a HTML file path here
  iframePath: '',

   // your file upload url, the return result must be a string refer to the uploaded image, leave it empty will end up with local preview
  fileuploadUrl: ''
};
// Vue.use(Vueditor, config);
// create a root instance
// new Vue({
//     el: '#editor1'
// });
Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(Vuetify);

// Vue.use(editor, options);
// Vue.use(VueHtml5Editor, options);

// Vue.use(VueHtml5Editor, {
//     //global component name
//     name: "vue-html5-editor",
//     //custom icon class of built-in modules,default using font-awesome
//     icons: {
//         text: "fa fa-pencil",
//         color: "fa fa-paint-brush",
//         font: "fa fa-font",
//         align: "fa fa-align-justify",
//         list: "fa fa-list",
//         link: "fa fa-chain",
//         unlink: "fa fa-chain-broken",
//         tabulation: "fa fa-table",
//         image: "fa fa-file-image-o",
//         hr: "fa fa-minus",
//         eraser: "fa fa-eraser",
//         undo: "fa-undo fa",
//         "full-screen": "fa fa-arrows-alt",
//         info: "fa fa-info",
//     },
//
//     //default en-us, en-us and zh-cn are built-in
//
//     //the modules you don't want
//     hiddenModules: [],
//     //keep only the modules you want and customize the order.
//     //can be used with hiddenModules together
//     visibleModules: [
//         "text",
//         "color",
//         "font",
//         "align",
//         "list",
//         "link",
//         "unlink",
//         // "tabulation",
//         // "image",
//         "hr",
//         "eraser",
//         "undo",
//         // "full-screen",
//         // "info",
//     ],
//     //extended modules
//     modules: {
//         //omit,reference to source code of build-in modules
//     }
// })



Vue.config.devtools = false;
export const bus = new Vue();

const router = new VueRouter({
  routes: Routes,
  mode: 'history'
});

// Vue.filter('contact', function(value) {
//   return value.replace(/##Contact us here##/g, "<a href='/contactus'>Contact Us</a>");
// });
Vue.filter('toUpperCase', function(value) {
  return value.toUpperCase();
});
Vue.filter('twoPlaces', function(value) {
  return value.toFixed(2);
});
Vue.filter('snippet', function(value) {
  // value = value.replace(" ", "_");
  // .slice(0,15);
  return value.replace(/,/g, "").replace(/'/g, "").replace(/ /g, "-").replace(':', '').toLowerCase();
});
Vue.filter('reverseSnippet', function(value) {
  return value.replace(/-/g, " ").toUpperCase();
});

Vue.filter('lineBreak', function(value) {       //<router-link to="/admin"  exact>Admin</router-link>
  value = value.replace(/Contact us here/g, '<span class="">Contact Us Modal</span>');
  return value.replace(/(?:\r\n|\r|\n)/g, '<br />');
});

new Vue({
  el: '#app',
  render: h => h(App),
  router: router,
  store
})
