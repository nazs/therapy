var sourceCode = false;
var isInEditMode = true;

function enableEdit() {
  richTextField.document.designMode = 'On';
}
function execCmd(cmd) {
  richTextField.document.execCommand(cmd, false, null);
}
function execCmdWithArg(cmd, arg) {
  richTextField.document.execCommand(cmd, false, arg);
}
function toggleSource() {
  if(sourceCode) {
    richTextField.document.getElementsByTagName('body')[0].innerHTML = richTextField.document.getElementsByTagName('body')[0].textContent;
    sourceCode = false;
  } else {
    richTextField.document.getElementsByTagName('body')[0].textContent = richTextField.document.getElementsByTagName('body')[0].innerHTML;
    sourceCode = true;
  }
}
function toggleEdit() {
  if(isInEditMode) {
    richTextField.document.designMode =  'Off';
    isInEditMode = false;
  } else {
    richTextField.document.designMode = 'On';
    isInEditMode = true;
  }
}

console.log('test from external .js');
